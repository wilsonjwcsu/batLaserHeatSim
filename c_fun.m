function c = c_fun(region,state)
    
    % tissue, brown fat
    % http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3824263/
    % data for human tissue; assuming it's similar to rodent tissue
    k(1) = 0.26e-6;    % thermal conductivity, W/(um K)

    % air at 20 �C
    % http://www.engineeringtoolbox.com/air-properties-d_156.html
    k(2) = 0.0257e-6;  % thermal conductivity, W/(um K)

    % glass
    % http://www.engineeringtoolbox.com/density-solids-d_1265.html
    % http://hyperphysics.phy-astr.gsu.edu/hbase/tables/thrcn.html
    % http://hyperphysics.phy-astr.gsu.edu/hbase/tables/sphtt.html
    k(3) = 0.8e-6;     % thermal conductivity, W/(um K)

    c = k(region.subdomain);