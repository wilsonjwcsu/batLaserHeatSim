function d = d_fun(region,state)

    % tissue, brown fat
    % http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3824263/
    % data for human tissue; assuming it's similar to rodent tissue
    rho(1) = 911e3/(1e6)^3;    % density of brown fat is 911 kg/m^3
    Cp(1)  = 2.503;            % specific heat, J/(g �C)

    % air at 20 �C
    % http://www.engineeringtoolbox.com/air-properties-d_156.html
    rho(2) = 1.205e3/(1e6)^3;  % density of glass ~ 1.2 kg/m^3
    Cp(2)  = 1.005;            % specific heat, J/(g �C)

    % glass
    % http://www.engineeringtoolbox.com/density-solids-d_1265.html
    % http://hyperphysics.phy-astr.gsu.edu/hbase/tables/thrcn.html
    % http://hyperphysics.phy-astr.gsu.edu/hbase/tables/sphtt.html
    rho(3) = 2.6e6/(1e6)^3;    % density of glass ~ 2.6e3 kg/m^3
    Cp(3)  = 0.84;             % specific heat, J/(g �C)

    d = rho(region.subdomain).*Cp(region.subdomain);