%% simulation of laser heating of brown adipose tissue
% Jesse Wilson (2016) Colorado State University
% jesse.wilson@colostate.edu
%
% This code based on the MATLAB documentation example here:
% http://www.mathworks.com/help/pde/examples/solving-a-heat-transfer-problem-with-temperature-dependent-properties.html

%% gaussian beam
NA = 0.15;
%w0 = 0.5; % waist
lambda = 0.532;   % wavelength (microns)
w0 = lambda/(pi*NA);
zR = pi*w0^2/lambda;    % Rayleigh range

% adjust these to match spot size
xmin = -10;
xmax = 10;
zmin = -60;
zmax = 60;
x = [xmin:0.1:xmax];
z = [zmin:0.1:zmax];


[X,Z] = meshgrid(x,z);

w_z = @(z) w0*sqrt(1+(z/zR).^2);
intens = @(x,z)(w0./w_z(z)).^2 .* exp(-2*x.^2 ./ w_z(z).^2);

imagesc(z,x,intens(X,Z).')
axis image
colormap default
title({'Focused beam intensity',sprintf('NA=%0.2f, w0=%0.1f ?m, ?=%0.3f ?m, z_R=%0.2f ?m',...
    NA, w0, lambda, zR)});
xlabel('z, ?m');
ylabel('x, ?m');



%% set up tissue slice square geometry
% 
model = createpde(1);
l = 20; % histology slice thickness, ?m

% air
r1 = [3 4 zmin -l/2 -l/2 zmin  xmin xmin xmax xmax];

% tissue
r2 = [3 4 -l/2 l/2 l/2 -l/2 xmin xmin xmax xmax];

% glass
r3 = [3 4 l/2 zmax zmax l/2 xmin xmin xmax xmax];

gdm = [r1; r2; r3]';
g = decsg(gdm);
clf
geometryFromEdges(model,g);
pdegplot(model,'edgeLabels','on','SubdomainLabels','on');
%axis([-.9 .9 -.9 .9]);
axis equal
title 'Block Geometry With Edge Labels Displayed'

%% boundary conditions: zero temp at edges
applyBoundaryCondition(model,'Edge',[1,2,4,5,6,7,8,9],'u',0);


%% generate sampling mesh
hmax = 1.5; % element size
msh=generateMesh(model,'Hmax',hmax);
pdeplot(model);
axis equal
title 'Block With Finite Element Mesh Displayed'

%% material properties for water
%rho(1) = 1e6/(1e6)^3;    % density of water is 1000kg/m^3 = 1e-12 g/?m^3
%Cp(1) = 4.186;     % specific heat, J/(g �C)
%k = 0.6e-6;      % thermal conductivity, W/(?m K)
%d = k/(rho*Cp); % diffusion constant

%% material properties for brown fat
% http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3824263/
% data for human tissue; assuming it's similar to rodent tissue
rho(1) = 911e3/(1e6)^3;    % density of brown fat is 911 kg/m^3
Cp(1) = 2.503; % specific heat, J/(g �C)
k(1) = 0.26e-6;     % thermal conductivity, W/(?m K)
d(1) = k(1)/(rho(1)*Cp(1)); % diffusion constant


%% material properties for air at 20 �C
% http://www.engineeringtoolbox.com/air-properties-d_156.html
rho(2) = 1.205e3/(1e6)^3; % density of glass ~ 1.2 kg/m^3
k(2) = 0.0257e-6; % thermal conductivity, W/(?m K)
Cp(2) = 1.005; % specific heat, J/(g �C)
d(2) = k(1)/(rho(1)*Cp(1)); % diffusion constant

%% material properties for glass
% http://www.engineeringtoolbox.com/density-solids-d_1265.html
% http://hyperphysics.phy-astr.gsu.edu/hbase/tables/thrcn.html
% http://hyperphysics.phy-astr.gsu.edu/hbase/tables/sphtt.html
rho(3) = 2.6e6/(1e6)^3; % density of glass ~ 2.6e3 kg/m^3
k(3) = 0.8e-6; % thermal conductivity, W/(?m K)
Cp(3) = 0.84; % specific heat, J/(g �C)
d(3) = k(1)/(rho(1)*Cp(1)); % diffusion constant


%% plot heating beam on mesh
zz = msh.Nodes(1,:);
xx = msh.Nodes(2,:);
u0 = intens(xx,zz);
pdeplot(model,'xydata',u0.','mesh','on')
axis image
colormap default


%% transient case with heating beam
t = 0:1e-4:10e-3;    % liste of time values, 0 to 5 sec
N = 3e5;    % number density of 0.5 mM cyt c, in 1/um^3
sigma = 1.13e-8;    % absorption cross-section, um^2
P0 = 2e-3;          % beam power, W
I0 = 2*P0/(pi*w0^2);    % intensity at focal spot, W/um^2
sourcefun = @(region,state) N*sigma*I0*intens(region.y, region.x).*(region.subdomain==1);
%sourcefun = @(region,state) (sqrt(region.x.^2 + (region.y+2).^2) <= 3).'*20.0;
%sourcefun = @(region,state) 1;
%sourcefun = @(region,state) region.x*0+1;
%sourcefun = @(region,state) (region.subdomain == 1).*1.0;

a = 0;
f = 0;

% tissue
c = k(1);
d = rho(1)*Cp(1);
specifyCoefficients(model,'m',0,'d',@d_fun,'c',@c_fun,'a',0,'f',sourcefun);
%
% air
%c = k(2);
%d = rho(2)*Cp(2);
%specifyCoefficients(model,'m',0,'d',d,'c',c,'a',a,'f',0,'face',2);

% glass
%c = k(3);
%d = rho(3)*Cp(3);
%specifyCoefficients(model,'m',0,'d',d,'c',c,'a',a,'f',0,'face',3);

setInitialConditions(model,0);  % all nodes at 0 temp initially?

%%
R = solvepde(model,t);
u= R.NodalSolution;

%% make a movie
clf;
for it = 1:1:length(t)
 %it = 2; 
    hold off
    pdeplot(model,'xydata',u(:,it),'contour','off');
    hold on
    pdegplot(model);
    colormap default
    axis image;
    %caxis([0,160]);
    title(sprintf('t = %0.2f ms',1000*t(it)));
    drawnow
end

%% plot temperature rise of center pixel
% (see http://www.mathworks.com/examples/pde/mw/pde_product-heatedBlockWithSlot-solving-a-heat-transfer-problem-with-temperature-dependent-properties)
getClosestNode = @(p,x,y) min((p(1,:) - x).^2 + (p(2,:) - y).^2);
[~,nid] = getClosestNode( msh.Nodes, 0, 0 );
clf
plot(t/1e-3,u(nid,:))
%ylim([0,1])
grid on;
ylabel('Temperature, arb.');
xlabel('Time, msec');

